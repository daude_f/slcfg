import argparse
import pathlib
import sys
import webbrowser

import coverage.config
import pytest


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--open-browser', action='store_true')
    parser.add_argument('--expects-full', action='store_true')
    params = parser.parse_args(sys.argv[1:])
    args = ['--cov=slcfg', '--cov-report=html']
    if params.expects_full:
        args.append('--cov-fail-under=100')
    status_code = pytest.main(args)
    if status_code == 0 and params.open_browser:
        coverage_config = coverage.config.read_coverage_config(
            config_file=True, warn=lambda _: None
        )
        assert coverage_config.config_file is not None
        report_location = (
            pathlib.Path(coverage_config.config_file).parent / coverage_config.html_dir
        )
        webbrowser.open(f'file://{report_location}/index.html')


if __name__ == '__main__':
    main()
