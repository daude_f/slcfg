import base64
import contextlib
import os
import pathlib
import uuid

import pytest

from slcfg import item, layer


def make_path(name: str):
    return pathlib.Path(f'{name}.txt')


@contextlib.contextmanager
def tmp_file(content: bytes):
    path = make_path(str(uuid.uuid4()))
    try:
        path.write_bytes(content)
        yield path
    finally:
        path.unlink(missing_ok=True)


def test_file():
    with tmp_file(content := b'abc') as path:
        file = layer.file_source(path, default=None)
        assert file.getter().read() == content


def test_optional_file():
    file = layer.file_source(make_path('does_not_exists'), default=b'default')
    assert file.getter().read() == b'default'


def test_missing_file():
    file = layer.file_source(make_path('does_not_exists_2'), default=None)
    with pytest.raises(FileNotFoundError):
        file.getter()


def test_json_file():
    with tmp_file(b'{"a": 1, "b": {"c": null}}') as path:
        assert list(layer.json_file_layer(path).getter()) == [
            item.Item(['a'], 1),
            item.Item(['b', 'c'], None),
        ]


TOML_CONTENT = b"""
a=1
[b]
c=2
"""


def test_toml_file():
    with tmp_file(TOML_CONTENT) as path:
        assert list(layer.toml_file_layer(path).getter()) == [
            item.Item(['a'], 1),
            item.Item(['b', 'c'], 2),
        ]


def test_env_layer_ci():
    os.environ['T_A'] = '1'
    os.environ['T_B_C'] = '2'
    os.environ['T_B_D'] = '3'
    layer_ = layer.env_layer(prefix='t_', nested_delimiter='_', case_sensitive=False)
    assert list(layer_.getter()) == [
        item.Item(['a'], '1'),
        item.Item(['b', 'c'], '2'),
        item.Item(['b', 'd'], '3'),
    ]


def test_env_layer_cs():
    os.environ['T'] = '1'
    os.environ['t_A_O_b'] = '2'  # noqa: SIM112
    os.environ['t_A_o_b'] = '3'  # noqa: SIM112
    layer_ = layer.env_layer(prefix='t_', nested_delimiter='_o_', case_sensitive=True)
    assert list(layer_.getter()) == [item.Item(['A_O_b'], '2'), item.Item(['A', 'b'], '3')]


def test_value_layer_1():
    layer_ = layer.value_layer({})
    assert list(layer_.getter()) == []


def test_value_layer_2():
    layer_ = layer.value_layer(1)
    assert list(layer_.getter()) == [item.Item([], 1)]


def test_value_layer_3():
    layer_ = layer.value_layer({'a': {'b': 1}, 'c': 2})
    assert list(layer_.getter()) == [item.Item(['a', 'b'], 1), item.Item(['c'], 2)]


def test_env_var_source():
    os.environ['A'] = '1'
    os.environ['B'] = '2'
    with contextlib.suppress(KeyError):
        del os.environ['C']
    assert layer.env_var_source('A', default=None).getter() == '1'
    assert layer.env_var_source('C', default='default').getter() == 'default'

    faulty_layer_ = layer.env_var_source('C', default=None)
    with pytest.raises(KeyError):
        faulty_layer_.getter()


def test_env_var_json():
    os.environ['A'] = base64.b64encode(b'{"a": 1}').decode()
    assert layer.env_base64_json_layer('A').getter() == [item.Item(['a'], 1)]


TOML_CONTENT_2 = b"""
a=1
"""


def test_env_var_toml():
    os.environ['A'] = base64.b64encode(TOML_CONTENT_2).decode()
    assert layer.env_base64_toml_layer('A').getter() == [item.Item(['a'], 1)]
