import dataclasses
import typing

from slcfg import config, item, layer


@dataclasses.dataclass
class Model:
    a: typing.Any
    b: typing.Any


def make_layer(data: object):
    return layer.Source(getter=lambda: item.list_items(data))


def test_config():
    assert config.read_config(Model, [make_layer({'a': 1, 'b': 1}), make_layer({'a': 2})]) == Model(
        2, 1
    )
    assert config.read_config(
        Model,
        [make_layer({'a': 1, 'b': {'c': 1}}), make_layer({'b': 2})],
        on_conflict=item.ConflictPolicy.OVERWRITE,
    ) == Model(1, 2)
